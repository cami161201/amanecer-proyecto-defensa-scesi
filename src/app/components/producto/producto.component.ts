import { Component, OnInit } from '@angular/core';
import { CatalogoService } from 'src/app/services/catalogo.service';
import { Producto } from 'src/app/interfaces/producto';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {
productos:Producto[]=[];
plantas:Producto[]=[];
  constructor(private catalogoService:CatalogoService) {
    console.log("constructor");
    
   }

  ngOnInit(): void {
    console.log("ngOninit");
    this.productos=this.catalogoService.getProductos();
    console.log(this.productos);
    this.plantas=this.catalogoService.getPlantas();
    
  }

}
