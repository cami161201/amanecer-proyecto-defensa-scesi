import { RouterModule, Routes } from '@angular/router';

import { InicioComponent } from './components/inicio/inicio.component';
import { NosotrosComponent } from './components/nosotros/nosotros.component';
import { ProductoComponent } from './components/producto/producto.component';


const routes: Routes = [
  { path: 'inicio', component: InicioComponent },
  { path: 'nosotros', component: NosotrosComponent },

  { path: 'productos', component: ProductoComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'inicio' },
];
export const APP_ROUTING = RouterModule.forRoot(routes);
