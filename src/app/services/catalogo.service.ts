import { Injectable } from '@angular/core';
import { Producto } from '../interfaces/producto';
@Injectable({
  providedIn: 'root',
})
export class CatalogoService {
  private productos: Producto[] = [
    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-594e9.appspot.com/o/comp1.jpg?alt=media&token=db860a9b-cbb6-494f-96eb-374ef66e8b97',
      nombre: 'Macetas',
      descripcion: 'lorem',
    },
    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-594e9.appspot.com/o/comp2.jpg?alt=media&token=b77dc288-fe6f-489b-81b1-58f5bf6a3ba0',
      nombre: 'Maceta 3d',
      descripcion: 'Maceta3D',
    },
    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-594e9.appspot.com/o/comp3.jpg?alt=media&token=7d2383c9-00d6-495f-9be5-4ce67f712cb5',
      nombre: 'Maceta de yeso',
      descripcion: 'lorem',
    },
    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-594e9.appspot.com/o/comp4.jpg?alt=media&token=5cf1d981-e7cc-4462-81a7-abbf08e5e6e2',
      nombre: 'Regaderas',
      descripcion: 'lorem',
    },
  ];
  private plantas: Producto[] = [
    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Productos%2Fcorona.jpg?alt=media&token=97f00b4c-35d5-4e05-8990-cb7014b77833',
      nombre: 'Corona de Cristo',
      descripcion:
        'Esta planta es originaria de la isla de Madagascar tiene flores muy llamativas y se utiliza como planta ornamental de jardín por su vistoso porte y floración. Esta planta requiere pocos cuidados, pero necesita calor y buen drenaje.',
    },
    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Productos%2FcactusOrquidea.jpg?alt=media&token=8b720c99-d413-4a7c-9cb0-55edd0968d14',
      nombre: 'Cactus Orquidea',
      descripcion:
        'Son cactus epífitos nativos de América Central se caracterizan por sus magníficas flores que se abren por la noche, son plantas de rápido crecimiento e ideales para cultivar en una maceta grande o en el tronco de un árbol.',
    },
    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Productos%2Fpeperomias.jpg?alt=media&token=6ce9b986-a5e8-4c7f-a55e-473754e50cae',
      nombre: 'Peperonias',
      descripcion:
        'La Peperomia es una planta siempre verde, de porte generalmente matoso, a menudo también trepador y rastrero que puede ser también cultivada en macetas suspendidas dando un precioso efecto decorativo',
    },

    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Productos%2Famerilis.jpg?alt=media&token=09bb5d1f-de85-4527-89ee-f9275dc2c18c',
      nombre: 'Amerilis',
      descripcion:
        'La amarilis es originaria de las zonas exóticas de América Central y del Sur, se desarrolla en espacios interiores y sorprende con sus hermosas flores, no tendrá que estar muy pendiente de ella, ya que es fácil de mantener.',
    },
    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Productos%2Fgirasoles.jpg?alt=media&token=d06a1869-0497-45f1-9f7e-e0b13565068a',
      nombre: 'Girasoles',
      descripcion:
        'El girasol proviene de Norteamérica y Sudamérica, las semillas de estas flores servían de alimento a los indios. Es una flor perfecta para decorar con flores en verano, porque inunda de color y alegría cualquier rincón con un estilo campestre.',
    },
    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Productos%2Fsucu.jpg?alt=media&token=9f3e26a6-3662-4006-b66b-69e7bed864b6',
      nombre: 'Suculentas',
      descripcion:
        'Las suculentas son muy populares debido a su fácil cultivo, poco mantenimiento y resistencia a la sequía. Estas plantas son originarias de zonas desérticas con lo que requieren poca agua y son perfectas para interior debido a ocupan poco espacio.',
    },
    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Productos%2Frosas.jpg?alt=media&token=23205e07-d966-40e2-84be-a683da35f89e',
      nombre: 'Rosas',
      descripcion:
        'Las rosas son las flores más populares en los jardines, están entre las flores más comunes vendidas por los floristas, es una planta decorativa y aromática. Necesitará sol, buen drenaje y buena circulación de aire',
    },
    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Productos%2Fespada.jpg?alt=media&token=64e0b3a6-400d-4113-87dd-5b14bfa7169c',
      nombre: 'Espada de San Jorge',
      descripcion:
        'La Espada de San Jorge es una planta originaria de África tropical, es una de las plantas de interior más resistente, tiene la capacidad de poder purificar el aire y sus cuidados son muy sencillos, además puede vivir en macetas toda su vida.',
    },
    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Productos%2Fcostillas.jpg?alt=media&token=62063498-8c17-4c50-9d99-852b6138a98f',
      nombre: 'Costillas de Adan',
      descripcion:
        'La Costilla de Adán es una planta muy popular que se caracteriza por tener unas grandes hojas verdes partidas, como si se tratase de unas costillas. Es una planta muy fácil de cuidar, no es muy exigente en riegos, es ideal para interior y exterior.',
    },

    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Productos%2Fepicias.jpg?alt=media&token=ca8ac249-a2c8-4b37-982a-9b36c3a9984b',
      nombre: 'Episcias',
      descripcion:
        'Las episcias son plantas tropicales rastreras de tallos largos de los que brotan hojas ovaladas que tiene unas atractivas flores que son tubulares y de color rojo. Se utilizan como plantas de interior en macetas colgantes o como centro de mesa.',
    },
    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Productos%2Fcorazon.jpg?alt=media&token=fb883eb4-7e1a-4f23-a89f-e6773cbb79af',
      nombre: 'Corazon de Jesus',
      descripcion:
        'El Caladium o Corazón de Jesús es excepcionalmente bonito, llamativo y sumamente exótico, es una planta que disfrutaremos por su espectacular porte. Y es que, más allá del colorido, si la cultivamos como necesita puede llegar a alcanzar los 50 cm.',
    },
    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Productos%2Fjalajala.jpg?alt=media&token=38d7a141-616a-4174-b96a-a07108687740',
      nombre: 'Cactus Jala Jala',
      descripcion:
        'Jala jala es un cactus nativo de Sudamérica (Argentina, Bolivia, Chile y Perú) tiene flores y espinas, para cualquier cactus la mejor ubicación es una en la que reciba muy buena luz natural directa en las horas de la mañana y muy buena ventilación.',
    },
    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Productos%2Fpino.jpg?alt=media&token=fc10657d-e264-43c6-ba9e-95583c9be541',
      nombre: 'Pino enano',
      descripcion:
        'El pino enano es una especie que se planta en maceta y que no suele alcanzar más de 3m de altura, es perfecto para cualquier espacio. Esta variedad del pino suele utilizarse para diseñar jardines y también queda muy bien en el interior del hogar.',
    },

    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Productos%2Fagabes.jpg?alt=media&token=e7efd34e-96d0-490c-82a0-119d4dba5baa',
      nombre: 'Agaves',
      descripcion:
        'El agave es una planta crasa, muy resistente a la sequía y fácil de cultivar ya que apenas requiere cuidados. Se suele cultivar en exterior, donde alcanza los 2m, pero también puede cultivarse en maceta. En ese caso, el tamaño es mucho más reducido.',
    },
    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Productos%2Fboas.jpg?alt=media&token=9e82581a-fd12-4685-98d0-f454c16c1116',
      nombre: 'Boas o potos',
      descripcion:
        'La planta boa o potos es una especie que no requiere muchos cuidados, se usan para interior y exterior, es una especie trepadora originaria de zonas tropicales de Asia y el Pacífico. Posee una alta resistencia a periodos de sequía y contaminación.',
    },
    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Productos%2Fcartucho.jpg?alt=media&token=09356b68-17a0-409b-9997-01b94aa10cca',
      nombre: 'Cartuchos',
      descripcion:
        'Los cartuchos o calas son nativas de sur de África desde Sudáfrica al norte de Malaui, se cultiva al exterior en sombra o semisombra como planta ornamental o para flor cortada por sus vistosas espatas de color blanco, resiste moderadamente el frio.',
    },
    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Productos%2Fbonsai.jpg?alt=media&token=0ba62531-882d-4123-9bcf-43ad8016d268',
      nombre: 'Bonsai',
      descripcion:
        'El Bonsái no es una planta genéticamente enana, de hecho, cualquier especie arbórea puede ser usada para formar uno. El objetivo final del Bonsái es crear una representación miniatura pero realista de una parte de la naturaleza, en este caso un árbol.',
    },
    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Productos%2Fcola-mono.jpg?alt=media&token=c628c8c6-a43e-4483-94e2-b2b757735b07',
      nombre: 'Cola de Mono',
      descripcion:
        'El cactus cola de mono es originario Bolivia, este cactus nació para ser colgado, es único y llama la atención por dos razones principales: sus grandes flores color rojo y sus espinas que parecen pelos debido a que son largas, blancas y suaves. ',
    },
    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Productos%2Fgenario.jpg?alt=media&token=cc6ce38d-b840-4317-b15e-09a993d04d95',
      nombre: 'Geranios',
      descripcion:
        'Los geranios son plantas de exterior con flores de colores vivos estos se suelen cultivar en el jardín pero también se pueden mantener en interior siempre y cuando, no se descuiden ciertos aspectos esta planta necesita mucha luz y no resiste el frío',
    },

    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Productos%2Fcactus.jpg?alt=media&token=ce91d9f9-905d-4be2-a329-958ab11f61c0',
      nombre: 'Cactus',
      descripcion:
        'Los cactus se caracterizan por acumular agua y nutrientes en sus tejidos, pueden adaptarse sin problema al hábitat en el que se encuentren. Son populares por su facilidad de cultivo, su bajo mantenimiento y el gran número de variedades disponibles. ',
    },

    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Productos%2Flirio.jpg?alt=media&token=cd7dc01f-857f-4be3-ab31-9a706dcdbfae',
      nombre: 'Lirio',
      descripcion: 'Los lirios, también llamados azucenas o iris, son una de las plantas ornamentales más apreciadas en todo el mundo, sobre todo gracias a sus variadas y grandes flores de colores vivos y algunas de agradable aroma, existen más de 100 variedades. ',
    },

    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Productos%2Fsuculentas.jpg?alt=media&token=d69b1374-6738-4f96-9cc6-873dc7f073c7',
      nombre: 'Echeverias',
      descripcion: 'La Echeveria es una planta ornamental de una extrema belleza gracias a sus hojas aplanadas en forma de roseta, puede tener distintas tonalidades y tamaños, son muy fáciles de cultivar debido a que resisten la falta de agua y tolera bien el frío.'
    },
    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Productos%2Faloe.jpg?alt=media&token=7cbee540-2ce0-4d5f-939a-58b67b979937',
      nombre: 'Aloe vera',
      descripcion: 'El Aloe vera, es una planta que proviene del norte del continente africano, existen más de 350 especies, se puede cultivar tanto en interior, como en exterior. Se trata de una planta muy similar al cactus, fácil de cuidar y con multitud de beneficios.',
    },
    {
      img: 'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Productos%2Fhelechos.jpg?alt=media&token=98d02e95-e138-4655-a3be-e8c1b415a1be',
      nombre: 'Helechos',
      descripcion: 'Los helechos son unas de las plantas más bonitas y populares para decorar gracias a que son plantas que dan una gran frescura al ambiente. Esta planta cuenta con una gran variedad de especies de distintas formas y tamaños, mayoría no superan los 2m.'
    },
  ];
  constructor() {}
  getProductos(): any[] {
    return this.productos;
  }
  getPlantas() {
    return this.plantas;
  }
}
