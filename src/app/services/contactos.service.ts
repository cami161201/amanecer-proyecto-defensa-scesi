import { Injectable } from '@angular/core';
import { Contacto } from '../interfaces/contacto';

@Injectable({
  providedIn: 'root',
})
export class ContactosService {
  private contactos: Contacto[] = [
    {
      nombre: 'Vivero Fernandez',
      img:"https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Vivero-Fernandez.jpeg?alt=media&token=7d2fda9a-0909-4775-9d6f-0e54ea85afdf",
      duenio: 'Carmen Fernandez Olguin',
      numero: '62675415',
      direccion: 'https://goo.gl/maps/8buMeYwBWqtyHGbq9',
      plantas:['Cartuchos amarillos','Calas','Suculentas']
    },
    // {
    //   nombre: 'Vivero Carmen',
    //   img:'https://firebasestorage.googleapis.com/v0/b/amanecer-594e9.appspot.com/o/IMG-20221007-WA0045.jpg?alt=media&token=cd243cd1-65af-4c24-8f6e-2f454ccc37b7',
    //   duenio: 'Carmen Ibarra',
    //   numero: '71045201',
    //   direccion: 'av.Blanco Galindo',
    //   plantas:['Plantas de interior']
    // },
    {
      nombre: 'Vivero SANDY',
      img:"https://firebasestorage.googleapis.com/v0/b/amanecer-594e9.appspot.com/o/IMG-20221009-WA0031.jpg?alt=media&token=d165f3bd-5944-46ff-a951-1592b96e5ac6",
      especialidad:'Coronas de cristo',
      duenio: 'Sandra Lovera',
      numero: '65323544',
      direccion: 'av.Blanco Galindo',
plantas:['Coronas de cristo',' Hoya',' Bambues']
    },
    {
      nombre: 'Vivero Delgadillo',
      img:"https://firebasestorage.googleapis.com/v0/b/amanecer-594e9.appspot.com/o/IMG-20221010-WA0050.jpg?alt=media&token=39337887-9526-4cb0-b8b9-343dda85e88a",
      duenio: 'Inocencia Delgadillo Vasquez',
      numero: '70793440',
      direccion: 'av.Blanco Galindo',
    },
    {
      nombre:'Vivero Soliz',
      duenio:'Cristina Soliz',
      numero:'67684376',
      img:'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Vivero-Soliz.jpeg?alt=media&token=dd738139-11ed-416a-906f-ab0dfadb3bf6',
      plantas:['Cactus orquidea']
    },
    {
      nombre:'Vivero Aguilar',
      duenio:'Luisa Albina Aguilar de Silez',
      numero:'60729904',
      img:'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Vivero-Aguilar.jpeg?alt=media&token=bcaa1e10-b7cc-4f49-b472-b93e5f87cd1c'
    },
    {
      nombre:'Retoños Bonsai',
      duenio:'Jherson Flores Condori',
      numero:'76478641',
      img:'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/retonio-bonsai.jpeg?alt=media&token=21c039ff-2a7b-48f1-b762-af646a8727ef',
      plantas:['Bonsai']
    },
    {
      nombre:'Vivero vida nueva',
      duenio:'Alfonso Toroya Marze',
      numero:'65326560',
      img:'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Vivero-vidaNueva.jpeg?alt=media&token=fdfcb5c5-05c5-490e-b213-010401a13566'
    },
    {
      nombre:"Vivero Waly's",
      duenio:'Waldo Arispe',
      numero:'75974236',
      img:'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Viero-Walys.jpeg?alt=media&token=288ef1a8-1563-4ff8-9e81-a878676024b8',
      plantas:['Suculentas',' Echeverias',' Agaves',' Aloes']
    },
    {
      nombre:'Vivero El Jardin de Eva',
      duenio:'Evangeline Garcia Mamani',
      numero:'77924942',
      img:'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/vivero-jardinEva.jpeg?alt=media&token=907e347f-11cc-42d1-825d-ea98af6da170',
      plantas:['Boa',' Corazon de Jesus',' Ameriles',' Epicias',' Peperonias']
    },
    {
      nombre:'Dreams 3D',
      duenio:'Kevin Acuña Jaldin Michael',
      numero:'65336733',
      img:'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Dreams-3d.jpeg?alt=media&token=5c8f2859-6309-4724-9413-a85f19afdd3c',
      plantas:['Macetas personalizadas hechas de PLA']
    },
    {
      nombre:'Vivero Silvia',
      duenio:'Silvia Roman',
      numero:'60701399',
      plantas:['Todo tipo de plantas de interior'],
      img:'https://firebasestorage.googleapis.com/v0/b/amanecer-c4154.appspot.com/o/Vivero-silvia.jpeg?alt=media&token=ff10766e-5ad9-4a72-84ee-dab226e23a27'
    }

  ];
  constructor() {}
  getContactos(): any[] {
    return this.contactos;
  }
}
