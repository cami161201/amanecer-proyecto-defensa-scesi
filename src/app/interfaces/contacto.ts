export interface Contacto{
  nombre?: string;
  img?:string;
  duenio?: string;
  especialidad?:string;
  numero?: string;
  direccion?: string;
  plantas?: string[];
}